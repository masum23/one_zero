
(function ($) {
    $(document).ready(function () {
        var scrollup_btn = $('.scrollup');
        var work=$('.our-work');
        var client=$('.our-client');
        // var main-nav=$('#main-nav');
        $(window).scroll(function () {
            var _t = $(this);
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;

            if (_t.scrollTop() > 200) {
                scrollup_btn.fadeIn();


            } else {
                scrollup_btn.fadeOut();


            }
        });
        scrollup_btn.click(function () {
            $("html,body").animate({
                scrollTop: 0
            }, 2000);
            return false;
        });
        work.click(function () {
            $('html, body').animate({
                scrollTop: $("#work1").offset().top
            }, 2000);
        });
        client.click(function () {
            $('html, body').animate({
                scrollTop: $("#client").offset().top
            }, 2000);
        });

    });
})(jQuery);
//For menu bg change
				$(window).on('scroll',function(){
				if ($(window).scrollTop()>250){
				$('#main-nav').addClass('menu-bg');
				$('#navbarSupportedContent').addClass('nav-item-margin');

				}else{
				$('#main-nav').removeClass('menu-bg');
				$('#navbarSupportedContent').removeClass('nav-item-margin');
				}
				});

$(document).ready(function () {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay:true,
        autoplaySpeed:false,
        autoplayTimeout:4000,
        slideTransition:"linear",
        slideBy:1,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })

});


$('.work').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    autoplay: true,
    autoplaySpeed: false,
    autoplayTimeout: 5000,
    slideBy: 1,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        },
        1400: {
            items: 4
        }
    }
});
$('.client').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    autoplay: true,
    autoplaySpeed: false,
    autoplayTimeout: 3000,
    slideBy: 1,
    dots:false,
    responsive: {
        0: {
            items: 1
        },
        500: {
            items: 2
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    }
});