#!/bin/bash

# run this script to compile sass to css for this project

# install sass: sudo apt-get install ruby-sass
# for short doc: sass -h


if [[ "$1" == "-c" ]]; then
    sass --watch sass:css --style compressed
  else
    sass --watch sass:css --style expanded
  fi
